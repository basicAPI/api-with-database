<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// Array containing names of PHP files to be included
$files = [
    'DatabaseConnection',
    'Task',
    'Menu',
    'Project',
    'Department',
    'Role',
    'User',
];

// Including required PHP files
foreach ($files as $f) {
    require_once "$f.php";
}

// Including Init.php
require_once 'Init.php';

// Assuming you are receiving JSON data in the POST request
$inputJSON = file_get_contents('php://input');

// Decoding JSON data into PHP associative array
$params = json_decode($inputJSON, true);

// Check if decoding was successful
if ($params === null) {
    // JSON decoding failed, setting error message
    $params = array('error' => 'Invalid request');
}

// Creating an instance of Init class with decoded parameters
$init = new Init($params);

// Starting the application and echoing the result
echo $init->start();

