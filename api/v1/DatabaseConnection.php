<?php

// DatabaseConnection class represents a connection to the database
class DatabaseConnection
{
    // Public property to hold the database connection instance
    // Accessible outside this class
    public $db_conn = NULL;

    /**
     * Constructor to establish the database connection.
     *
     * @param string $mysqlHost The MySQL host.
     * @param string $mysqlPort The MySQL port.
     * @param string $mysqlUser The MySQL user.
     * @param string $mysqlPassword The MySQL password.
     * @param string $database The database name.
     * 
     * @return void
     */
    public function __construct($mysqlHost, $mysqlPort, $mysqlUser, $mysqlPassword, $database)
    {
        try {
            // Attempt to establish a PDO connection to the database
            $db_conn = new PDO("mysql:host=$mysqlHost; port=$mysqlPort; dbname=$database", $mysqlUser, $mysqlPassword);
        } catch (PDOException $ex) {
            // If connection fails, show error message
            $this->showErrorMessage($ex->getMessage());
        }
        // Set error mode to exception for PDO
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // Assign the established connection to class property
        $this->db_conn =  $db_conn;
    }

    /**
     * Method to show error message when connection fails.
     * 
     * @param string $message The error message to display.
     * 
     * @return void
     */
    public static function showErrorMessage($message)
    {
        // Display error message and instructions for the user
        echo ('<h3 style="color:#ff0000; text-align:center"></br>' .$message) . '</br></br>=>Contact the administrator if you are  the system user for help.</br>=>If you are the administrator, this message may come due to misconfiguration in the host machine that authenticate this system.</br>OR</br> The connection to the database server may be unavailable!</h3>';
        echo '<h3 style="text-align:center"></br></br><img src="images/back.gif" style="cursor:pointer" alt="Return"/></br>Click <a href="./">here</a> to return</h3>';
        // Terminate script execution
        exit();
    }

    /**
     * Static method to generate a general error message.
     * 
     * @param int $error_code The error code.
     * 
     * @return string Returns a general error message.
     */
    public static function getGeneralError($error_code)
    {
        return "General Error (".$error_code.")";
    }
}

?>
