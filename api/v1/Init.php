<?php

class Init
{
    private $params;
    private $db_conn;

    /**
     * Constructor to initialize class properties.
     *
     * @param array $params Parameters for initializing the class.
     * 
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
        // Get database connection
        $this->db_conn = $this->getDatabaseConnection()->db_conn;
    }

    /**
     * Method to start the application.
     * 
     * @return mixed Returns the result of the requested route.
     */
    public function start()
    {
        try {
            // Execute the requested route
            return $this->{$this->params["route"]}();
        } catch (\Throwable $th) {
            // Handle any exceptions
            // Return error message as JSON
            return json_encode(["error"=>$th]);
        }
    }

    /**
     * Method to get a database connection.
     * 
     * @return DatabaseConnection Returns a DatabaseConnection object.
     */
    private function getDatabaseConnection()
    {
        // Read database configuration from file
        $config = json_decode(file_get_contents('config.json'), true);

        // Create and return a new DatabaseConnection instance
        return new DatabaseConnection(
            $config['host'],
            $config['port'],
            $config['username'],
            $config['password'],
            $config['database']
        );
    }

    /**
     * Method to create an instance of a specified entity class.
     * 
     * @param string $entityClass The class name of the entity.
     * 
     * @return mixed Returns an instance of the specified entity class.
     */
    private function createEntityInstance($entityClass)
    {
        // Create and return a new instance of the specified entity class
        return new $entityClass($this->db_conn, $this->params);
    }

    /**
     * Method to handle user-related operations.
     * 
     * @return string Returns the result of user-related operations.
     */
    public function user()
    {
        // Create a User instance and execute user-related operations
        return $this->createEntityInstance(User::class)->exec();
    }
}
