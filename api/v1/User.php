<?php

// Including required PHP file
require_once "UserDao.php";


// User class represents a user entity
class User
{
    // Private properties
    private $db_conn;
    private $action;
    private $params;

    /**
     * Constructor to initialize class properties.
     *
     * @param PDO $conn The PDO database connection.
     * @param array $params Parameters for initializing the User class.
     * 
     * @return void
     */
    public function __construct($conn, $params)
    {
        $this->db_conn = $conn;
        $this->action = $params["action"];
        $this->params = $params;
    }

    /**
     * Method to execute user-related actions.
     * 
     * @return string Returns a JSON-encoded string representing the result of user-related actions.
     */
    public function exec()
    {
        // Create an instance of UserDao
        $userDao = new UserDao($this->db_conn);

        // Switch statement to determine action
        switch ($this->action) {
            // Action: Get all users
            case "all":
                return json_encode($userDao->getAll());
                break;
            
            // Action: Get user by ID
            case "id":
                return json_encode($userDao->get($this->params));
                break;

            // Action: Insert new user
            case "i":
                return json_encode($userDao->insert($this->params));
                break;

            // Action: Update user
            case "u":
                return json_encode($userDao->update($this->params));
                break;

            // Default case: Invalid action
            default:
                return json_encode(["error" => "Invalid action"]);
                break;
        }
    }
}


