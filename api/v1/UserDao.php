<?php

// UserDao class represents data access operations for User entities
class UserDao
{
    // Private property to hold the database connection instance
    private $db_conn;

    /**
     * Constructor to initialize the database connection.
     *
     * @param PDO $conn The PDO database connection.
     * 
     * @return void
     */
    public function __construct($conn)
    {
        $this->db_conn = $conn;
    }

    /**
     * Method to get a user by ID.
     *
     * @param array $params Parameters for retrieving a user by ID.
     * 
     * @return array Returns an array containing user information.
     */
    public function get($params)
    {
        $sql = "CALL uspUser_Get(?)";
        $stmt = $this->db_conn->prepare($sql);
        $stmt->bindParam(1, $params["id"]);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Method to get all users.
     *
     * @return array Returns an array containing information of all users.
     */
    public function getAll()
    {
        $sql = "CALL uspUser_GetAll()";
        $stmt = $this->db_conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Method to insert a new user.
     *
     * @param array $params Parameters for inserting a new user.
     * 
     * @return array Returns an array containing information about the inserted user.
     */
    public function insert($params)
    {
        $sql = "CALL uspUser_Insert(?, ?, ?, ?, ?)";
        $stmt = $this->db_conn->prepare($sql);
        $stmt->bindParam(1, $params["uname"]);
        $stmt->bindParam(2, $params["fname"]);
        $stmt->bindParam(3, $params["sname"]);
        $stmt->bindParam(4, $params["role_id"]);
        $stmt->bindParam(5, $params["by"]);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Method to update a user.
     *
     * @param array $params Parameters for updating a user.
     * 
     * @return array Returns an array containing information about the updated user.
     */
    public function update($params)
    {
        $sql = "CALL uspUser_Update(?, ?)";
        $stmt = $this->db_conn->prepare($sql);
        $stmt->bindParam(1, $params["id"]);
        $stmt->bindParam(2, $params["name"]);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Method to delete a user.
     *
     * @param array $params Parameters for deleting a user.
     * 
     * @return array Returns an array containing information about the deleted user.
     */
    public function delete($params)
    {
        $sql = "CALL uspUser_Delete(?)";
        $stmt = $this->db_conn->prepare($sql);
        $stmt->bindParam(1, $params["id"]);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
